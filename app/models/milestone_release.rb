# frozen_string_literal: true

class MilestoneRelease < ApplicationRecord
  extend SuppressCompositePrimaryKeyWarning

  belongs_to :milestone
  belongs_to :release

  validate :same_project_between_milestone_and_release
  validate :same_group_between_milestone_and_release

  private

  def same_project_between_milestone_and_release
    return if milestone&.project_id == release&.project_id

    return if milestone&.group_id

    errors.add(:base, _('Release does not have the same project as the milestone'))
  end

  def same_group_between_milestone_and_release
    return unless release_id_changed? || milestone_id_changed?
    return unless milestone&.group_id && release&.project_id

    g = release.project.group
    while g do
      return if g.id == milestone.group_id
      g = g.parent
    end

    errors.add(:base, _('Associated project not member of the milestone\'s group'))
  end
end

MilestoneRelease.prepend_mod_with('MilestoneRelease')
